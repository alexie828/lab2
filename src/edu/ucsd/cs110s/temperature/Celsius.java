/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author cs110sar
 *
 */
public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	}
	
	public String toString()
	{
		//TODO: Complete this method
		return Float.toString(this.getValue());
	}

	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return this;
	}

	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		float temp = this.getValue();
		temp = (float) (temp) * ((float)9 / 5) + 32;
		Temperature fah = new Fahrenheit(temp);
		return fah;
	}
}
