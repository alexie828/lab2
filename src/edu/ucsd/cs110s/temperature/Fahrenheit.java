/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author cs110sar
 *
 */
public class Fahrenheit extends Temperature{
	public Fahrenheit(float t) 
	{
		super(t);
	}
	public String toString()
	{
		//TODO: Complete this method
		String output = Float.toString(getValue());
		return output;
	}
	@Override
	public Temperature toCelsius() {
		float f = getValue();
		float c = (f - 32) * ((float)5 / (float) 9);
		Temperature output = new Celsius(c);
		return output;
	}
	@Override
	public Temperature toFahrenheit() {
		return this;
	}
}
